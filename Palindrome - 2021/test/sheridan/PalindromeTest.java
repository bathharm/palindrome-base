package sheridan;

import static org.junit.Assert.*;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		boolean isPalindrome = Palindrome.isPalindrome("racecar");
		assertTrue( "Unable to validate", isPalindrome);
	}

	@Test
	public void testIsPalindromeNeg( ) {
		boolean isPalindrome = Palindrome.isPalindrome("harman");
		assertFalse( "Unable to validate", isPalindrome);
	}
	@Test
	public void testIsPalindromeBIn( ) {
		boolean isPalindrome = Palindrome.isPalindrome("race car");
		assertTrue( "Unable to validate", isPalindrome);
	}
	@Test
	public void testIsPalindromeBOut( ) {
		boolean isPalindrome = Palindrome.isPalindrome("race a car");
		assertFalse( "Unable to validate", isPalindrome);
	}
	
}
